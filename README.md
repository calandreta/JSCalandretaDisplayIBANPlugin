********************************************
*                                          *
*         Quick start instructions         *
*                                          *
********************************************

JSCalandretaDisplayIBANPlugin is a plugin for the "Planeta" software ( https://framagit.org/calandreta/planeta ).

To install this plugin :
- open the /Common/Config.php file of "Planeta"
- go to the $CONF_JS_PLUGINS_TO_USE variable
- before the '/Support/Canteen/CanteenPlanning.php' entry, paste the following code :

'/Support/Canteen/FamilyDetails.php' => array(
                                              'initCalandretaDisplayIBANPlugin' => array(
                                                                                'Scripts' => array(
                                                                                                   $CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/JSCalandretaDisplayIBANPlugin.js"
                                                                                                  ),
                                                                                'Params' => array($CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/", $CONF_LANG),
                                                                                'Css' => array(
                                                                                               'screen' => array($CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/JSCalandretaDisplayIBANPluginStyles.css")
                                                                                              )
                                                                               )
                                              ),
                                 '/Support/Canteen/UpdateFamily.php' => array(
                                              'initCalandretaDisplayIBANPlugin' => array(
                                                                                'Scripts' => array(
                                                                                                   $CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/JSCalandretaDisplayIBANPlugin.js"
                                                                                                  ),
                                                                                'Params' => array($CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/", $CONF_LANG),
                                                                                'Css' => array(
                                                                                               'screen' => array($CONF_ROOT_DIRECTORY."Plugins/JSCalandretaDisplayIBANPlugin/JSCalandretaDisplayIBANPluginStyles.css")
                                                                                              )
                                                                               )
                                              ),

- save the modification done

Now, the users with a "family" account can view the IBAN of the Calandreta by click on the "IBAN" icon from their "Family details" page ("bills" part).