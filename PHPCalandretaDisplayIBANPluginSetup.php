<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * JS Calandreta plugin IBAN module : display or not the IBAN icon to download the IBAN
 * of the Calandreta in relation with profil of the connected user
 *
 * @author Christophe Javouhey
 * @version 4.0
 *     - 2023-09-01 : v4.0. Use $CONF_CHARSET
 *
 * @since 2020-07-01
 */


 // Include Config.php because of the name of the session
 require_once dirname(__FILE__).'/../../Common/DefinedConst.php';
 require_once dirname(__FILE__).'/../../Common/Config.php';
 require_once dirname(__FILE__).'/../../GUI/GiXMLLibrary.php';

 session_start();

 $XmlData = '';
 if (isSet($_SESSION["SupportMemberID"]))
 {
     // We analyse the parameters of the request

     //########################### GET parameters #############################
     $ArrayParams = array();

     if (array_key_exists('getDisplay', $_GET))
     {
         $ArrayParams['Display'] = 1;
     }

     if (array_key_exists('getSupportMemberStateID', $_GET))
     {
         $ArrayParams['SupportMemberStateID'] = $_SESSION['SupportMemberStateID'];
     }

     if (!empty($ArrayParams))
     {
         $XmlData = xmlOpenDocument();
         $XmlData .= xmlTag("Setup", "", $ArrayParams);
         $XmlData .= xmlCloseDocument();
     }
 }
 else
 {
     $XmlData = xmlOpenDocument();
     $XmlData .= xmlCloseDocument();
 }

 header('Content-type: application/xml; charset='.strtolower($CONF_CHARSET));
 echo $XmlData;
?>
