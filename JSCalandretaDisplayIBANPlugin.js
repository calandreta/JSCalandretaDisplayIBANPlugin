/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * JS Calandreta plugin IBAN module : display a IBAN icon to download the IBAN
 * of the Calandreta in relation with profil of the connected user
 *
 * @author Christophe Javouhey
 * @version 4.0
 * @since 2020-07-01
 */


 var CalandretaDisplayIBANPluginPath;
 var CalandretaDisplayIBANPluginLanguage;
 var CalandretaDisplayIBANPluginAjax;


/**
 * Function used to init this plugin
 *
 * @author Christophe Javouhey
 * @version 1.1
 *     - 2023-09-01 : v1.1. Remove Path parameter
 *
 * @since 2020-07-01
 */
 function initCalandretaDisplayIBANPlugin(Lang)
 {
     CalandretaDisplayIBANPluginPath = CmnLibGetJSPath('JSCalandretaDisplayIBANPlugin');
     CalandretaDisplayIBANPluginLanguage = Lang;

     // We check if we can display the IBAN icon for the logged user
     if(window.XMLHttpRequest) // Firefox
         CalandretaDisplayIBANPluginAjax = new XMLHttpRequest();
     else if(window.ActiveXObject) // Internet Explorer
         CalandretaDisplayIBANPluginAjax = new ActiveXObject("Microsoft.XMLHTTP");
     else { // XMLHttpRequest non support� par le navigateur
         alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
     }

     CalandretaDisplayIBANPluginAjax.onreadystatechange = CalandretaDisplayIBANPluginGetSetupXML;
     CalandretaDisplayIBANPluginAjax.open("GET", CalandretaDisplayIBANPluginPath + "PHPCalandretaDisplayIBANPluginSetup.php?getDisplay=1&getSupportMemberStateID=1", true);
     CalandretaDisplayIBANPluginAjax.send(null);
 }


// Get the display mode
 function CalandretaDisplayIBANPluginGetSetupXML()
 {
     if ((CalandretaDisplayIBANPluginAjax.readyState == 4) && (CalandretaDisplayIBANPluginAjax.status == 200)) {
         var DocXML = CalandretaDisplayIBANPluginAjax.responseXML.documentElement;
         var items = DocXML.childNodes;

         // Setup variables
         var CalandretaDisplayIBANPluginSetupDisplay = 0;
         var CalandretaDisplayIBANPluginSetupSupportMemberStateID = 0;

         if (items.length > 2) {
             for(var i = 0; i < items.length; i++) {
                 if (items[i].nodeName == 'setup') {
                     // Display variable
                     if (items[i].getAttribute('display') >= 0) {
                         CalandretaDisplayIBANPluginSetupDisplay = items[i].getAttribute('display');
                     }

                     // SupportMemberStateID variable
                     if (items[i].getAttribute('supportmemberstateid') > 0) {
                         CalandretaDisplayIBANPluginSetupSupportMemberStateID = items[i].getAttribute('supportmemberstateid');
                     }
                 }
             }

             if (CalandretaDisplayIBANPluginSetupDisplay >= 1) {
                 // We can display the IBAN icon in relation with the profil of the logged supporter
                 switch(CalandretaDisplayIBANPluginSetupSupportMemberStateID)
                 {
                     case '2':
                     case '5':
                         // Display IBAN icon for these user profils
                         CalandretaDisplayIBANPluginDisplayIcon(CalandretaDisplayIBANPluginSetupSupportMemberStateID);
                         break;

                     default:
                         // Do nothing
                         break;
                 }
             }
         }
     }
 }


 function CalandretaDisplayIBANPluginDisplayIcon(SupportMemberStateID)
 {
     // Define messages
     var sMsgTip = '';
     switch(CalandretaDisplayIBANPluginLanguage) {
         case 'fr':
             sMsgTip = "Cliquer pour t�l�charger le RIB de l'association Calandreta de Mureth.";
             break;

         case 'oc':
             sMsgTip = "Clicar per telecargar lo RIB del Calandreta del Pa�s Murethin.";
             break;

         default:
             sMsgTip = "Click to download the IBAN of the Calandreta.";
             break;
     }

     // Create the IBAN icon
     var objIBANArea = document.createElement('div');
     objIBANArea.setAttribute('id', 'IBAN');

     var objImg = document.createElement('img');
     objImg.setAttribute('src', CalandretaDisplayIBANPluginPath + 'IBANIcon.png');
     objImg.setAttribute('title', sMsgTip);
     objImg.setAttribute('alt', sMsgTip);

     var objLink = document.createElement('a');
     objLink.setAttribute('href', CalandretaDisplayIBANPluginPath + 'IBAN_Calandreta_Mureth.pdf');
     objLink.setAttribute('target', '_blank');
     objLink.appendChild(objImg);

     objIBANArea.appendChild(objLink);

     var objBillsList = document.getElementById('FamilyBillsList');
     if (objBillsList) {
         // Display IBAN icon near contextual menu
         objBillsList.getElementsByTagName('td')[0].appendChild(objIBANArea);
     }
 }



